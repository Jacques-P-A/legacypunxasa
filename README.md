A multiplayer Rover demo app.   

# Purpose
The purpose of this repo is to have a blank legacy MRTK application configured with PUN2 and ASA, using Unity v2019.4.28f1 and MRTK Foundation v2.7.0, which demonstrably built onto Hololens 2, in order to be able to begin projects more quickly.
## Source
See [this tutorial](https://docs.microsoft.com/en-us/windows/mixed-reality/develop/unity/tutorials/mr-learning-sharing-01) to build this project from scratch.


# How to use this repo
1. Clone it
2. Open the project using Unity Hub
    - When you open the project you may be given the option to configure the project XR settings; choose Legacy XR
3. Deploy the app to Hololens 2
4. Run the app on either two Hololens or a Hololens and a computer to see the other user moving the rover around 
## Caveats
- this application will throw a non-breaking error about missing an Azure dll file on the PC when run in the Unity Editor. This is normal; the application is meant to be run with a pair of Hololens2 devices, and not a Hololens and a PC.

# Useful info
## Azure
### Global project config
Instead of setting the Spatial Anchors Account ID and Key in the scene, you can set it for your entire project, this can be advantageous if you have multiple scenes using ASA. To do this, in the Project window, navigate to the Assets > AzureSpatialAnchors.SDK > Resources > SpatialAnchorConfig asset, then set the values of your ASA resource in the Inspector window.

# Debugging tips
## PUN Problems
### The connection is made, but there is still a connection timeout?!
I was struggling with this for like a day, there was a connection to the exitgames server, but there was always eventually a connection timeout. My solution to this was to switch to the CAS guest network, for which PUN works. I think it must be security on the CAS network interfering with PUN.  